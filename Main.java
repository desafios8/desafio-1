import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		String cotacao;
		String novo;
		int op = 0;
		Scanner entrada = new Scanner(System.in);
		Menu menu = new Menu();
		
		while(op!=5) 
		{
			System.out.println("Escolha uma das op��es");
			System.out.println("1-Cadastrar nova cota��o");
			System.out.println("2-Remover cota��o");
			System.out.println("3-Renomear cota��o");
			System.out.println("4-Imprimir");
			System.out.println("5-Sair");
			op = entrada.nextInt();
			if(op==1) 
			{
				System.out.println("Digite a cota��o para ser adicionada");
				cotacao = entrada.next();
				menu.adicionar(cotacao);
			}
			
			if(op==2) 
			{
				System.out.println("Digite a cota��o para ser removida");
				cotacao = entrada.next();
				menu.removerCotacao(cotacao);
			}
			
			if(op==3) 
			{
				System.out.println("Digite a cota��o para ser renomeada");
				cotacao = entrada.next();
				System.out.println("Digite o novo nome para a cota��o");
				novo = entrada.next();
				menu.alterarTitulo(cotacao,novo);
			}
			
			if(op==4) 
			{
				menu.imprimir();
			}
			
		}

	}

}
