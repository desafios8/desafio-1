import java.util.ArrayList;

public class Menu {
ArrayList<String> c = new ArrayList();
	
	public void adicionar(String cotacao) 
	{
		c.add(cotacao);
	}
	
	public void removerCotacao(String cotacao) 
	{
		if(c.contains(cotacao)==true) {
			c.remove(cotacao);
		}
		else System.out.println("Cotacao n�o existe");
	}
	
	public void alterarTitulo(String cotacao, String novo) 
	{
		removerCotacao(cotacao);
		adicionar(novo);
	}
	
	public void imprimir() 
	{
		for(int i = 0;i<c.size();i++) 
		{
			System.out.println(c.get(i));
		}
	}

}
